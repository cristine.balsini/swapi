import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

const Links = () => {
  return (
    <div className="links">
      <Link style={{ color: "red" }} to="/">
        HOME
      </Link>
      <Link style={{ color: "red" }} to="/people">
        People
      </Link>
      <Link style={{ color: "red" }} to="/vehicles">
        Vehicles
      </Link>
      <Link style={{ color: "red" }} to="/planets">
        Planets
      </Link>
    </div>
  );
};

export default Links;
