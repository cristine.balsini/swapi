import React from "react";
import { Switch, Route } from "react-router-dom";
import Homepage from "../../homepage";
import People from "../../people";
import Vehicles from "../../vehicles";
import Planets from "../../planets";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Homepage />
      </Route>
      <Route exact path="/people">
        <People />
      </Route>
      <Route exact path="/vehicles">
        <Vehicles />
      </Route>
      <Route exact path="/planets">
        <Planets />
      </Route>
    </Switch>
  );
};

export default Routes;
