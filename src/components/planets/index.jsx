import React, { useState, useEffect } from "react";
import Location from "./location";
import { Card, Typography, Input } from "antd";

const Planets = () => {
  const { Title } = Typography;
  const { Meta } = Card;
  const [name, setName] = useState([]);
  const [planet, setPlanet] = useState([]);
  const [nextUrl, setNextUrl] = useState([
    "https://swapi.dev/api/planets/?page=1",
  ]);

  const getPlanet = () => {
    fetch(nextUrl)
      .then((resp) => resp.json())
      .then((resp) => {
        setPlanet([...planet, ...resp.results]); //aqui concatena!

        setNextUrl(
          resp.next !== null ? resp.next.replace("http", "https") : ""
        );
      });
  };

  useEffect(getPlanet, [nextUrl]); //aqui para atualizar para nextUrl

  const getName = (e) => {
    setName(e.target.value);
  };
  const findPlanet = planet.find((item) => item.name === name); //aqui encontra o match do input

  return (
    <>
      <Title level={3}>Busque seu planeta pelas galáxias:</Title>
      <Input placeholder="digite aqui" style={{ width: 300, backgroundColor: "black", color: "white" }} value={name} onChange={getName} />
      {findPlanet && (
        <div>
          <Card title={findPlanet.name}>
            <Meta
              description={`Climate: ${findPlanet.climate} ## Terrain: ${findPlanet.terrain} ## Population: ${findPlanet.population}`}
            />
          </Card>
        </div>
      )}

      {!findPlanet && <Location listPlanet={planet} />}
    </>
  );
};

export default Planets;
