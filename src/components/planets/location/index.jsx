import React from "react";
import "antd/dist/antd.css";
import { Card } from "antd";

const Location = (props) => {
  const { Meta } = Card;
  return (
    <div>
      {props.listPlanet.map((item, index) => (
        <Card title={item.name} key={index}>
          <Meta
            description={`Climate: ${item.climate} ## Terrain: ${item.terrain} ## Population: ${item.population}`}
          />
        </Card>
      ))}
    </div>
  );
};

export default Location;
