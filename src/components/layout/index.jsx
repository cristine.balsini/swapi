import React from "react";
import "antd/dist/antd.css";
import "./style.css";
import { Layout } from "antd";
import Links from "../pages/links";
import Routes from "../pages/routes";

const Layouts = () => {
  const { Header, Footer, Sider, Content } = Layout;
  return (
    <Layout>
      <Sider>
        <Links />
      </Sider>
      <Layout>
        <Header>DESAFIO SWAPI</Header>
        <Content>
          <Routes />
        </Content>
        <Footer>
          <p>Kenzie Academy Brazil</p>
        </Footer>
      </Layout>
    </Layout>
  );
};

export default Layouts;
