import React from "react";
import "antd/dist/antd.css";
import { Card } from "antd";

const Character = (props) => {
  const { Meta } = Card;
  return (
    <div>
      {props.peopleList.map((item, index) => (
        <Card title={item.name} key={index}>
          <Meta
            description={`Birthday: ${item.birth_year} ## Gender: ${item.gender} ## Hair color: ${item.hair_color}`}
          />
        </Card>
      ))}
    </div>
  );
};

export default Character;
