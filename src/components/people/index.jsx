import React, { useState, useEffect } from "react";
import Character from "./character";
import { Card, Typography, Input } from "antd";

const People = () => {
  const { Meta } = Card;
  const { Title } = Typography;
  const [name, setName] = useState([]);
  const [people, setPeople] = useState([]);
  const [nextUrl, setNextUrl] = useState([
    "https://swapi.dev/api/people/?page=1",
  ]);

  const getPeople = () => {
    fetch(nextUrl)
      .then((resp) => resp.json())
      .then((resp) => {
        setPeople([...people, ...resp.results]);
        setNextUrl(
          resp.next !== null ? resp.next.replace("http", "https") : ""
        );
      });
  };
  useEffect(getPeople, [nextUrl]);

  const getName = (e) => {
    setName(e.target.value);
  };
  const findCharacter = people.find((item) => item.name === name);

  return (
    <>
      <Title level={3}>Selecione seu personagem favorito:</Title>
      <Input
        placeholder="digite aqui"
        style={{ width: 300, backgroundColor: "black", color: "white" }}
        value={name}
        onChange={getName}
      />
      {findCharacter && (
        <div>
          <Card title={findCharacter.name}>
            <Meta
              description={`Birthday: ${findCharacter.birth_year} ## Gender: ${findCharacter.gender} ## Hair color: ${findCharacter.hair_color}`}
            />
          </Card>
        </div>
      )}

      {!findCharacter && <Character peopleList={people} />}
    </>
  );
};

export default People;
