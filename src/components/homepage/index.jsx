import React from "react";
import "./style.css";
import { Typography } from "antd";

const Homepage = () => {
  const { Title } = Typography;
  return (
    <div className="main-home">
      <Title>
        {" "}
        "Você descobrirá que muitas das verdades às quais nos prendemos <br />
        dependem do nosso ponto de vista."
      </Title>
    </div>
  );
};

export default Homepage;
