import React from "react";
import "antd/dist/antd.css";
import { Card } from "antd";

const Car = (props) => {
  const { Meta } = Card;
  return (
    <div>
      {props.listVehicle.map((item, index) => (
        <Card title={item.name} key={index}>
          <Meta
            description={`Model: ${item.model} ## Manufacturer: ${item.manufacturer}`}
          />
        </Card>
      ))}
    </div>
  );
};

export default Car;
