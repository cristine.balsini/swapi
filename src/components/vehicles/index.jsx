import React, { useState, useEffect } from "react";
import Car from "./car";
import { Card, Typography, Input } from "antd";

const Vehicles = () => {
  const { Meta } = Card;
  const { Title } = Typography;
  const [name, setName] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [nextUrl, setNextUrl] = useState([
    "https://swapi.dev/api/vehicles/?page=1",
  ]);
  console.log(nextUrl);
  const getVehicles = () => {
    fetch(nextUrl)
      .then((resp) => resp.json())
      .then((resp) => {
        setVehicles([...vehicles, ...resp.results]);
        setNextUrl(
          resp.next !== null ? resp.next.replace("http", "https") : ""
        );
      });
  };

  useEffect(getVehicles, [nextUrl]);

  const getName = (e) => {
    setName(e.target.value);
  };
  const findCar = vehicles.find((item) => item.name === name);

  return (
    <>
      <Title level={3}>Escolha o modelo de sua preferência:</Title>
      <Input
        placeholder="digite aqui"
        style={{ width: 300, backgroundColor: "black", color: "white" }}
        value={name}
        onChange={getName}
      />
      {findCar && (
        <div>
          <Card title={findCar.name}>
            <Meta
              description={`Model: ${findCar.model} ## Manufacturer: ${findCar.manufacturer}`}
            />
          </Card>
        </div>
      )}

      {!findCar && <Car listVehicle={vehicles} />}
    </>
  );
};

export default Vehicles;
