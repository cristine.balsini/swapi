import React from "react";
import "./App.css";
//import "./App.css";
import Layouts from "./components/layout";

function App() {
  return (
    <div className="App-header">
      <Layouts />
    </div>
  );
}

export default App;
